# PPL ASSIGNMENT

## Website Link
- Production: <https://ppl-ticketeers-psi.vercel.app/>

## Sonarqube Status
[![Quality Gate Status](https://sonarqube.cs.ui.ac.id/api/project_badges/measure?project=ppldticketeers&metric=alert_status)](https://sonarqube.cs.ui.ac.id/dashboard?id=ppldticketeers)

## Pipeline Status
- Main  
    [![main pipeline status](https://gitlab.com/mrizkyca/ppl-ticketeers/badges/main/pipeline.svg)](https://gitlab.com/mrizkyca/ppl-ticketeers/-/commits/main)